var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/:name', function (req, res, next) {
  let name = req.params.name.split(".")[0]
  res.render(name, { title: 'Express' }, (err, html) => {
    if (err) {
      console.log("Got Error");
      res.redirect('/');
    }
    res.send(html);
    console.log("No Error");
  });
});

module.exports = router;
