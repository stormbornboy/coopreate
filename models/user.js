
const Joi = require('joi');
const mongoose = require("mongoose");
// const passportLocalMongoose = require("passport-local-mongoose");

var bcrypt = require('bcryptjs');

mongoose.connect('mongodb://localhost/nodeauth');

var db = mongoose.connection;

// Book Schema
var BookSchema = mongoose.Schema({
  Name: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    minlength: 5,
    maxlength: 255
  },
  ImagePath: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 1024
  },
  Category: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    minlength: 5,
    maxlength: 255
  },
  CreatedAt: {
    type: Date,
    required: false,
    default: Date.now,
    minlength: 5,
    maxlength: 50
  },
  meta: {
    views: Number,
    borrows: Number
  }
});

var Book = module.exports = mongoose.model('Book', BookSchema);

module.exports.getBookById = function (id, callback) {
  Book.findById(id, callback);
}

module.exports.getBookByBookname = function (Bookname, callback) {
  var query = { Bookname: Bookname };
  Book.findOne(query, callback);
}

module.exports.comparePassword = function (candidatePassword, hash, callback) {
  bcrypt.compare(candidatePassword, hash, function (err, isMatch) {
    callback(null, isMatch);
  });
}

module.exports.createBook = function (newBook, callback) {
  bcrypt.genSalt(10, function (err, salt) {
    bcrypt.hash(newBook.password, salt, function (err, hash) {
      newBook.password = hash;
      newBook.save(callback);
    });
  });
}

module.exports.validateBook = function validateBook(Book) {
  const schema = {
    name: Joi.string().min(5).max(50).required(),
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(255).required()
  };

  return Joi.validate(customer, schema);
}





// const Book = mongoose.model('Book', new mongoose.Schema({
//     name: {
//       type: String,
//       required: true,
//       minlength: 5,
//       maxlength: 50
//     },  
//     Bookname : {
//        type: String,
//        required : true,
//        minlength : 5,
//        maxlength : 50,
//        trim: true,
//     },
//     email : {
//        type: String,
//        required : true,
//        trim: true,
//        unique: true,
//        minlength : 5,
//        maxlength : 255
//     }, 
//     password : {
//         type : String,
//         required : true,
//         minlength : 5,
//         maxlength : 1024
//     },
//     secret: String,
//     isAdmin : {type : Boolean, default : false},

//   }));

//   function validateBook(Book) {
//     const schema = {
//       name: Joi.string().min(5).max(50).required(),
//       email: Joi.string().min(5).max(255).required().email(),
//       password: Joi.string().min(5).max(255).required()
//     };

//     return Joi.validate(customer, schema);
//   }



// module.exports =  mongoose.model("Book", BookSchema);
// exports.Book = Book; 
// exports.validate = validateBook;