
const Joi = require('joi');
const mongoose = require("mongoose");
// const passportLocalMongoose = require("passport-local-mongoose");

var bcrypt = require('bcryptjs');

mongoose.connect('mongodb://localhost/nodeauth');

var db = mongoose.connection;

// User Schema
var UserSchema = mongoose.Schema({
    username: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50,
        trim: true,
        index: true
    },
    password: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 1024
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        minlength: 5,
        maxlength: 255
    },
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    }
});

var User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function (id, callback) {
    User.findById(id, callback);
}

module.exports.getUserByUsername = function (username, callback) {
    var query = { username: username };
    User.findOne(query, callback);
}

module.exports.comparePassword = function (candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, function (err, isMatch) {
        callback(null, isMatch);
    });
}

module.exports.createUser = function (newUser, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(newUser.password, salt, function (err, hash) {
            newUser.password = hash;
            newUser.save(callback);
        });
    });
}

module.exports.validateUser = function validateUser(user) {
    const schema = {
        name: Joi.string().min(5).max(50).required(),
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(5).max(255).required()
    };

    return Joi.validate(customer, schema);
}





// const User = mongoose.model('User', new mongoose.Schema({
//     name: {
//       type: String,
//       required: true,
//       minlength: 5,
//       maxlength: 50
//     },  
//     username : {
//        type: String,
//        required : true,
//        minlength : 5,
//        maxlength : 50,
//        trim: true,
//     },
//     email : {
//        type: String,
//        required : true,
//        trim: true,
//        unique: true,
//        minlength : 5,
//        maxlength : 255
//     }, 
//     password : {
//         type : String,
//         required : true,
//         minlength : 5,
//         maxlength : 1024
//     },
//     secret: String,
//     isAdmin : {type : Boolean, default : false},

//   }));

//   function validateUser(user) {
//     const schema = {
//       name: Joi.string().min(5).max(50).required(),
//       email: Joi.string().min(5).max(255).required().email(),
//       password: Joi.string().min(5).max(255).required()
//     };

//     return Joi.validate(customer, schema);
//   }



// module.exports =  mongoose.model("User", userSchema);
// exports.User = User; 
// exports.validate = validateUser;